package org.kurczynski.binarytree.structure;

public class Node implements Comparable<Node> {
	private Node parent;
	private Node left;
	private Node right;

	private final int value;

	public Node(int value) {
		this.value = value;
		this.parent = null;
		this.left = null;
		this.right = null;
	}

	public int getValue() {
		return this.value;
	}

	public Node getParent() {
		return parent;
	}

	private void setParent(Node parent) {
		this.parent = parent;
	}

	public Node getLeft() {
		return left;
	}

	void setLeft(Node left) {
		left.setParent(this);
		this.left = left;
	}

	public Node getRight() {
		return right;
	}

	void setRight(Node right) {
		right.setParent(this);
		this.right = right;
	}

	@Override
	public int compareTo(Node node) {
		return Integer.compare(this.value, node.value);
	}

	@Override
	public String toString() {
		return String.valueOf(this.value);
	}

}
