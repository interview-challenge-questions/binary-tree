package org.kurczynski.binarytree.structure;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class TreeStore {
	private final Map<Long, Tree> store;
	private static TreeStore instance;
	private static final AtomicLong counter = new AtomicLong();

	private TreeStore() {
		this.store = new HashMap<>();
	}

	public static TreeStore getInstance() {
		if (TreeStore.instance == null) {
			return new TreeStore();
		} else {
			return TreeStore.instance;
		}
	}

	public Tree getTree(long id) {
		return this.store.get(id);
	}

	public Tree createTree() {
		long id = TreeStore.counter.incrementAndGet();
		Tree newTree = new Tree(id);

		this.store.put(id, newTree);

		return newTree;
	}
}
