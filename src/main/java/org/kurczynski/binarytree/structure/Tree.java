package org.kurczynski.binarytree.structure;

public class Tree {
	private final long id;
	private Node root;

	public Tree(long id) {
		this.root = null;
		this.id = id;
	}

	Node getRoot() {
		return root;
	}

	public long getId() {
		return id;
	}

	public void add(Node node) {
		if (this.root == null) {
			this.root = node;
		} else {
			this.addNode(this.root, node);
		}
	}

	private Node addNode(Node current, Node newNode) {
		if (current == null) {
			return newNode;
		} else if (newNode.compareTo(current) < 0) {
			current.setLeft(this.addNode(current.getLeft(), newNode));
		} else if (newNode.compareTo(current) > 0) {
			current.setRight(this.addNode(current.getRight(), newNode));
		}

		return current;
	}
}
