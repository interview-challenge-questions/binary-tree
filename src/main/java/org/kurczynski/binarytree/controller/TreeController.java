package org.kurczynski.binarytree.controller;

import org.kurczynski.binarytree.structure.Node;
import org.kurczynski.binarytree.structure.Tree;
import org.kurczynski.binarytree.structure.TreeStore;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TreeController {
	@RequestMapping(path = "/tree", method = RequestMethod.POST)
	public Tree createTree() {
		TreeStore treeStore = TreeStore.getInstance();

		return treeStore.createTree();
	}

	@RequestMapping(path = "/tree", method = RequestMethod.PATCH)
	public void addNode(@RequestParam(value = "id") long id, @RequestParam(value = "value") int value) {
		TreeStore treeStore = TreeStore.getInstance();

		Tree tree = treeStore.getTree(id);
		tree.add(new Node(value));
	}

	@RequestMapping(path = "/tree", method = RequestMethod.GET)
	public Tree getTree(@RequestParam(value = "id") long id) {
		TreeStore treeStore = TreeStore.getInstance();
		
		return treeStore.getTree(id);
	}
}
