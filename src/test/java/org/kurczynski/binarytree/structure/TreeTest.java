package org.kurczynski.binarytree.structure;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TreeTest {
	Tree tree;

	@BeforeEach
	void setUp() {
		this.tree = new Tree(1);
	}

	@Test
	void shouldAddNodeAsRootToEmtpyTree() {
		Node node = new Node(1);

		this.tree.add(node);

		assertEquals(node, this.tree.getRoot());
	}

	@Test
	void shouldAddNodeAsLeafToNonEmptyTree() {
		Node root = new Node(10);
		Node smaller = new Node(2);
		Node larger = new Node(73);

		this.tree.add(root);
		this.tree.add(smaller);
		this.tree.add(larger);

		assertEquals(root, this.tree.getRoot());
		assertEquals(smaller, this.tree.getRoot().getLeft());
		assertEquals(larger, this.tree.getRoot().getRight());
	}
}
